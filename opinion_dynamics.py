#!/usr/bin/env python

##########################################################################################
# This python program implements a simple model of univariate opinion dynamics
###########################################################################################

# TODO: separate plotting functions into separate class?
# TODO: write function that extracts all the kwargs (same piece of code repeated for several differen functions)

# Advanced TODO: make graph dynamical (changing with time)



from __future__ import division
from __future__ import print_function

import numpy as np
from numpy import tanh, sign
from scipy.signal import argrelextrema
import scipy.linalg as spla

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import rc
from matplotlib import colors

from pylab import * #for movies
import time #for movies
import matplotlib.animation as animation #for movies

import datetime as dt

import random as ra

import sys

import networkx as nx 

from sklearn.cluster import MeanShift, estimate_bandwidth, KMeans
from sklearn.neighbors import KernelDensity
from sklearn.metrics import silhouette_score

from itertools import groupby

# from types import ModuleType
# try:
#     from importlib import reload  # Python 3.4+
# except ImportError:
#     # Needed for Python 3.0-3.3; harmless in Python 2.7 where imp.reload is just an
#     # alias for the builtin reload.
#     from imp import reload


### CUSTOM MODULES ###
import random_connected_graph as rcg
import utils

__author__      =   "Paolo Molignini"
__copyright__   =   "Copyright 2022, University of Stockholm"
__version__     =   "1.0.1"


####################################
######       REFERENCES       ######
####################################
# https://www.americansurveycenter.org/research/the-state-of-american-friendship-change-challenges-and-loss/ 



####################################
######     GLOBAL OBJECTS     ######
####################################
custom_ops = np.array([0.1, 0.14, 0.18, 0.23, 0.27, 0.35, 0.46, 0.58, 0.78, 0.98])





######################################################################################
class OpinionDynamics:
    """
    Creates an object codifying an opinion dynamics problem.

    Attributes:
    -----------
    N_actors, int: 
        the number of actors in the considered set.
    opinions, numpy array: 
        the opinions at each time step.
    traits, numpy array: 
        agreeableness, neuroticism, etc. for each agent and each time step.
    graph, graph object: 
        graph object (separate class) that describes the underlying connectivity among actors.
    adj, numpy array: 
        same as graph, but written as adjacency matrix.
    initial_distr, str: 
        specifies the initial conditions.
    graph_type, str: 
        controls the connectivity of the graph
    bond_distr, str: 
        controls the strength of the edges in the graph.
    bond_type, str: 
        controls whether graph is directed/undirected

    Methods:
    --------

    
    Notes:
    ------
    The graph attribute only describes the connectivity. For simplicity, the strength of the bond is
    encoded in the adjacency matrix.

    """

    ######################################################################################
    def __init__(self, 
                 N_actors: int,
                 **kwargs):
        
        """
        Parameters:
        ----------
        N_actors, int: the number of actors in the considered set.



        """
        self.verbose = kwargs.get("verbose", False)
        self.plot_flag = kwargs.get("plot_flag", True)

        ######################################
        # OPINION & TRAIT INITIALIZATION 
        ######################################
        self.a_min = kwargs.get("a_min", None)
        self.a_max = kwargs.get("a_max", None)
        self.r_min = kwargs.get("r_min", None)
        self.r_max = kwargs.get("r_max", None)
        self.n_min = kwargs.get("n_min", None)
        self.n_max = kwargs.get("n_max", None)
        self.static_traits = kwargs.get("static_traits", None)
        self.trait_input_check(**kwargs)
        self.initial_distr = kwargs.get("initial_distr", "uniform")

        # Initialize agent properties:
        self.N_actors = N_actors
        self.opinions = self.initialize_opinions()
        if kwargs.get("update_rule")=="SABER":
            self.traits = self.initialize_traits()
        ######################################


        #########################################
        # GRAPH & ADJACENCY MATRIX INITIALIZATION 
        #########################################
        # Check for custom input graphs or adjacency matrices:
        input_graph = kwargs.get("graph", None)
        input_adj = kwargs.get("adj", None)
        self.graph = None
        self.adj = None
        self.static_graph = kwargs.get("static_graph", None)
        self.graph_method = kwargs.get("graph_method", None)
        if input_graph is None and input_adj is None:
            print("No pre-loaded graph or adjacency matrix detected. Assembling graph from scratch...")

            self.graph_input_check(**kwargs)

            # Connectivity initialization:
            self.graph = self.initialize_graph()
            self.adj = self.initialize_adj()

            # Plot of the graph/adjacency matrix:
            if self.plot_flag==True:
                self.plot_graph(circular=False)
                self.plot_adj()

        #TODO: currently graph/adj input implies being directly a graph object or a numpy array.
        # Change to path of the file and loading from file in the future.
        elif input_graph is not None and input_adj is None:
            print("Pre-loaded graph detected.")
            self.graph = input_graph
            self.adj = self.initialize_adj()

        elif input_graph is None and input_adj is not None:
            print("Pre-loaded adjacency matrix detected.")
            self.adj = input_adj
            self.graph = self.initialize_graph()

        else:
            raise ValueError("Please input either graph or adjacency matrix, not both. I get confused otherwise.")
        
        #########################################


        #########################################
        # DYNAMICS INITIALIZATION
        #########################################
        # Initialize parameters for the dynamics
        self.eps = kwargs.get("eps", None)
        self.delta = kwargs.get("delta", None)
        self.kappa = kwargs.get("kappa", None)
        self.update_rule = kwargs.get("update_rule", None)
        self.t_final = kwargs.get("t_final", None)
        self.convergence_thresh = kwargs.get("convergence_thresh", None)

        self.dynamics_input_check()    
        #########################################

    ######################################################################################

    ######################################################################################
    def reset_dynamics(self):
        """
        Resets the dynamics to initial conditions.
        """
        self.opinions = self.initialize_opinions()
        if self.update_rule=="SABER":
            self.traits = self.initialize_traits()

        return
    ######################################################################################

    ######################################################################################
    def reset_graph(self):
        """
        Resets the graph for random initializations.
        """
        self.graph = None
        self.adj = None
        self.graph = self.initialize_graph()
        self.adj = self.initialize_adj()

        if self.plot_flag==True:
            self.plot_graph(circular=False)
            self.plot_adj()            
        return
    ######################################################################################

    ######################################################################################
    def get_all_attributes(self):
        """
        Print all the attributes of the current instance of the class.
        """
        raise NotImplementedError()

    ######################################################################################

    ######################################################################################
    def trait_input_check(self, **kwargs):

        # Check parameters:
        variables = [self.a_min, self.a_max, self.r_min, self.r_max, self.n_min, self.n_max]
        variable_names = ["a_min", "a_max", "r_min", "r_max", "n_min", "n_max"]
        for idx, i in enumerate(variables):
            if kwargs.get("update_rule")=="SABER" and i is None:
                raise ValueError(f"{variable_names[idx]} was not provided. This input parameter is necessary for trait initialization!")
        
        if kwargs.get("initial_distr", None) is None:
            print("initial_distr was not provided. This flag controls the initial conditions for the opinions. Using default initial_distr=uniform!")

        if self.static_traits is None:
            print("Flag \"static_traits\" was not provided. This flag controls the trait dynamics. Using default static_traits=True.")
            self.static_traits=True

        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print("Agent trait input check passed.")
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        return 
    
    ######################################################################################

    ######################################################################################
    def graph_input_check(self, **kwargs):

        # Check flags:

        # GRAPH TYPE:
        self.graph_type = kwargs.get("graph_type", None)
        if self.graph_type is None:
            print("Flag \"graph_type\" was not provided. This flag controls the geometry of the graph. Using default graph_type=complete (fully connected graph)!")
            self.graph_type = "complete"

        elif self.graph_type=="simply-connected-random":  
            self.N_edges = kwargs.get("N_edges", None)
            if self.N_edges is None: 
                print(f"No number of edges provided. Using default N_edges=N_actors={self.N_actors}.")
                self.N_edges = self.N_actors

            self.graph_method = kwargs.get("graph_method", None)
            if self.graph_method is None: 
                print(f"No graph assembling method provided. Using default: naive.")
                self.graph_method = "naive"
            elif self.graph_method!="partition" and self.graph_method!="Wilson" and self.graph_method!="random_walk":
                raise ValueError(f"Flag \"graph_method\"={self.graph_method} not recognized!")


        elif self.graph_type!="uniform-random" and self.graph_type!="2cluster" and self.graph_type!="complete":
            raise ValueError(f"Flag \"graph_type\"={self.graph_type} not recognized!")


        # LOOPS
        self.loops = kwargs.get("loops", None)
        if self.loops is None:
            print("Flag \"loops\" was not provided. This flag controls whether the nodes in the graph have loops (self-edges). Using default loops=False (no self-edges)!")
            self.loops = False

        # BOND_DISTR
        self.bond_distr = kwargs.get("bond_distr", None)
        if self.bond_distr is None:
            print("Flag \"bond_distr\" was not provided. This flag controls the values of the bonds (edges in the graph). Using default bond_distr=ones!")
            self.bond_distr = "ones"

        elif self.bond_distr!="ones" and self.bond_distr!="uniform" and self.bond_distr!="uniform+noise" and self.bond_distr!="random":    
            raise ValueError(f"Flag \"bond_distr\"={self.bond_distr}  not recognized!")



        # BOND_TYPE
        self.bond_type = kwargs.get("bond_type", None)
        if self.bond_type is None:
            print("Flag \"bond_type\" was not provided. This flag determines whether the graph has directed or undirected edges. Using default bond_type=undirected!")
            self.bond_type = "undirected"
        
        elif self.bond_type!="directed" and self.bond_type!="undirected":
            raise ValueError(f"Flag \"bond_type\"={self.bond_type} not recognized!")

        self.col_stoch = kwargs.get("col_stoch", None)
        if self.col_stoch is None:
            print("Flag \"col_stoch\" was not provided. This flag determines whether the adjacency matrix is column stochastic or not. Using default col_stoch=False!")
            self.col_stoch = False

        if self.col_stoch==True and self.bond_distr=="ones":
            print("You have chosen bond_distr=ones which is incompatible with defining a column-stochastic adjacency matrix. I will set col_stoch=False.")
            self.col_stoch = False
        
        if self.col_stoch==True and self.bond_type=="undirected":
            print("You have chosen bond_type=undirected which is incompatible with defining a column-stochastic adjacency matrix. I will set col_stoch=False.")
            self.col_stoch = False

        if self.col_stoch==True and self.bond_distr=="uniform+noise":
            print("You have chosen bond_distr=uniform+noise and col_stoch=True. In small systems, enforcing column stochasticity might interfere with the noise added to the bonds!")

        if self.graph_type=="complete" and self.bond_type=="directed":
            print("You have chosen a complete graph, which at present cannot be defined as directed graph. I will set bond_type=undirected!")

        if self.static_graph is None:
            print("Flag \"static_graph\" was not provided. This flag controls the graph dynamics. Using default static_graph=True.")
            self.static_graph=True

        print("!!!!!!!!!!!!!!!!!!!!!!!!!")
        print("Graph input check passed.")
        print("!!!!!!!!!!!!!!!!!!!!!!!!!")

        return 
    
    ######################################################################################

    ######################################################################################
    def dynamics_input_check(self):
        if self.update_rule=="smooth-HK" and self.kappa is None:
            print("You have chosen a smoothened Hegselmann-Krause update rule, but the smoothening parameter kappa was not provided. I will set it to the default kappa=1000.")
            self.kappa = 1000
        
        if self.eps is None:
            print("You have not provided a value for the threshold epsilon. I will set it to the default eps=0.5")
            self.eps = 0.5

        if self.eps is None:
            print("You have not provided a value for the threshold delta. I will set it to the default delta=0.0 (no repulsion).")
            self.delta = 0.0

        if self.update_rule is None:
            print("You have not provided an update rule for the dynamics! I will set it to the default update_rule=HK.")
            self.update_rule="HK"
        
        elif self.update_rule!="SABER" and self.update_rule!="HK" and self.update_rule!="HK-smooth":
            raise ValueError(f"Flag \"update_rule\"={self.update_rule} not recognized!")
        
        if self.t_final is None:
            if self.convergence_thresh is not None:
                print(f"You have not provided a value for the final time in the time evolution! The criterion to stop the dynamics is reaching the convergence threshold {self.convergence_thresh}")
            else: 
                print(f"You have not provided a value for the convergence threshold! I will use the default 1e-12")
            self.convergence_thresh = 1e-12

        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print("Dynamics input check passed.")
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

        return
    ######################################################################################



    ######################################################################################
    def initialize_opinions(self):
        """
        Initializes the opinion values for each actor.

        Notes:
        ------
        A list containing a single numpy array is returned, because it makes it easier
        to simply append more arrays for the time evolution.
    
        """

        opinions = np.zeros(self.N_actors)
        if self.initial_distr=="custom":
            opinions = custom_ops

        else:
            for i in range(self.N_actors):
                if self.initial_distr=="fully_random":
                    opinions[i] = ra.uniform(0.0,1.0)
                elif self.initial_distr=="uniform":
                    opinions[i] = 0.0 + 1.0/(self.N_actors-1)*i
                    
        return [opinions]
    
    ######################################################################################

    ######################################################################################
    def initialize_traits(self):
        """
        Initializes the trait values for each actor.

        Notes:
        ------
        A list containing a single numpy array is returned, because it makes it easier
        to simply append more arrays for the time evolution.
    
        The traits to retrieve the standard Hegselmann-Krause model are:
        
        a_min=1.0, a_max=1.0, r_min=1.0, r_max=1.0, n_min=0.0, n_max=0.0


        """
        traits = np.zeros(shape=(self.N_actors, 3))

        for i in range(self.N_actors):
            
            # index 0 is for agreeableness 
            # (controls the confidence radius in the update) 
            traits[i,0] = ra.uniform(self.a_min,self.a_max)

            # index 1 is for receptivness (how easily the actor changes opinion)
            traits[i,1] = ra.uniform(self.r_min,self.r_max)
            
            # index 2 is for neuroticism (de-facto repulsive term)
            traits[i,2] = ra.uniform(self.n_min,self.n_max)

        return [traits]
    ######################################################################################

    ######################################################################################
    def initialize_graph(self, plot=False, **kwargs):
        """
        Initializes the connectivity graph based on the input.
    
        """

        graph = None

        # Build graph from adjacency matrix:
        if self.adj is not None:
            graph = rcg.graph_from_adj(self.adj)
            return graph
        
        # Creation of a fully random but simply connected graph:
        if self.graph_type=="simply-connected-random":
            graph = rcg.simply_connected_graph(N_nodes=self.N_actors,
                                               N_edges=self.N_edges,
                                               loops=self.loops,
                                               bond_type=self.bond_type,
                                               method=self.graph_method)

        elif self.graph_type=="complete":
            graph = rcg.complete_graph(N_nodes=self.N_actors,
                                       loops=self.loops)
            
        elif self.graph_type=="2cluster":
            graph = rcg.two_cluster_graph(N_nodes=self.N_actors,
                                          loops=self.loops,
                                          bond_type=self.bond_type,
                                          verbose=self.verbose,
                                          )
        elif self.graph_type=="uniform-random":
            graph = rcg.uniform_random_graph(N_nodes=self.N_actors,
                                          loops=self.loops,
                                          bond_type=self.bond_type,
                                          verbose=self.verbose,
                                          )
        
        return graph
    ######################################################################################


    ######################################################################################
    def initialize_adj(self):

        if self.graph is not None:
                adj = self.get_adjacency_matrix_from_graph()
        else:
            print("Adjacency matrix initialization from empty graph not implemented yet!")
            sys.exit()

        return adj
    ######################################################################################


    ######################################################################################
    def get_adjacency_matrix_from_graph(self):
        """
        
        Parameters:
        -----------
        graph, custom Graph class: object containing the information about the graph.

        Notes:
        ------
        The edges are obtained from the attribute graph.edges.

        """
        edges = self.graph.edges
        adj = np.zeros(shape=(self.N_actors,self.N_actors))

        for edge in edges:
            
            from_node = edge[0]
            to_node = edge[1]

            if self.verbose==True:
                print(f"from node: {from_node}")
                print(f"to node: {to_node}")
            
            # Calculate how many other nodes point to the "to_node":
            # (this number is given by the number of nonzero entries in the to_node-th column 
            # of the adjacency matrix and can be used for both directed and undirected graphs
            # because of the way the edges are saved in the graph object)
            s = len([t[0] for t in self.graph.edges if t[1] == to_node])

            if self.bond_distr=="ones":
                bond=1

            elif self.bond_distr=="uniform":
                bond = 1/s

            elif self.bond_distr=="uniform+noise":
                bond = 1/s + ra.gauss(0,0.05)
            
            elif self.bond_distr=="random":
                bond = ra.uniform(0.0,1.0)

            adj[from_node, to_node] = bond

        # If the graph is not directed, add the transpose to the adjacency matrix
        # to account for both directions
        if self.graph.digraph == False:
            for i in range(self.N_actors):
                for j in range(self.N_actors):
                    if adj[i,j] != adj[j,i] and adj[i,j]!=0:
                        adj[j,i] = adj[i,j] 


        # Normalize every column such that it sums to one (this means that the nodes can influence a given node
        # with a weight up to one).
        if self.col_stoch==True:
            for col in range(np.shape(adj)[1]):
                colsum = np.sum(adj[:,col])
                for row in range(np.shape(adj)[0]):
                    if colsum != 0:
                        adj[row,col] /= colsum


        return adj

    ######################################################################################


    ######################################################################################
    def plot_graph(self, circular):
        """
        Plots the graph via the adjacency matrix.

        Notes:
        ------
        We go through the adjacency matrix because the graph object is not a networkx graph object,
        so it cannot be plotted directly with the networkx functions.

        """

        G = nx.DiGraph()
        for i in range(np.shape(self.adj)[0]): 
            for j in range(np.shape(self.adj)[1]): 
                if self.adj[i][j] != 0: 
                    G.add_edge(i, j, weight=self.adj[i][j])

        edges, weights = zip(*nx.get_edge_attributes(G, 'weight').items())
                        
        if circular==True:
            pos=nx.circular_layout(G)
        else:
            pos=nx.spring_layout(G)

        nx.draw(G, 
                pos=pos, 
                node_color='b', 
                font_color='w', 
                edge_color=weights, 
                width=2, 
                with_labels=True,
                edge_cmap=mpl.cm.get_cmap('plasma'),
                vmin=0.0,
                vmax=1.0)

        plt.show()
        plt.close()
        
        return
    ######################################################################################

    ######################################################################################
    def plot_adj(self):
        """
        Plots the adjacency matrix.

        """

        fig = plt.figure()
        ax = fig.add_subplot(111)
        if self.col_stoch==True:
            plot = ax.imshow(self.adj, cmap="plasma", vmin=0.0, vmax=1.0)
        else:
            plot = ax.imshow(self.adj, cmap="plasma")
        ax.set_xlabel("Agent")
        ax.set_ylabel("Agent")
        plt.colorbar(plot)
        plt.show()
        plt.close()
        
        return
    ######################################################################################

    ######################
    ####   DYNAMICS    ###
    ######################
    ######################################################################################
    def time_step_opinions_and_traits(self,
                                      timestep):

        # All opinions:
        #print(self.opinions[timestep])
        all_ops = self.opinions[timestep]
        # Array where to store the opinions at the next time step:
        next_opinions = np.zeros(self.N_actors)
        next_traits = np.zeros(shape=(self.N_actors,3))

        for i in range(self.N_actors):
            oi = self.opinions[timestep][i]   # opinion of i-th agent
            if self.verbose==True:
                print("\n")
                print(f"Considering actor {i} with opinion {oi}")

            # find i's neighbors:
            # TODO: extend to case where adjacency matrix changes with time, too.
            #print(self.adj)
            all_neigh = self.adj[:,i]
            all_neigh_mask = self.adj[:,i] == 0   # select the zero columns to mask (= to ignore)
            # calling masking function to get a masked array for the
            # opinion of the neighbors
            neigh_ops = np.ma.array(all_ops, mask=all_neigh_mask).filled(fill_value=0.0)
            if self.verbose==True:
                print(f"Opinions of all neighbors: {neigh_ops}")
            
            if self.update_rule == "SABER" or self.update_rule == "HK":
                # Of the neighbors' opinions, we consider the ones that are
                # within an eps away from the opinion of the i-th actor (i.e. we mask away
                # those >= eps):
                eps_ops = np.abs(all_ops - oi) >= self.eps
                eps_neigh = np.ma.array(neigh_ops, mask=eps_ops).filled(fill_value=0.0)
                eps_weights = np.ma.array(all_neigh, mask=eps_ops).filled(fill_value=0.0)
                if self.verbose==True:
                    print(f"Opinions of all neighbors within epsilon: {eps_weights}")

                # get average value of the opinions within eps, weighted by the *bonds* of the 
                # corresponding neighbors:
                o_eps = eps_weights@eps_neigh/sum(eps_weights)                 
                if self.verbose==True:
                    print(f"They have an average opinion of: {o_eps}")

            # Assign next opinions for a simple Hegselmann-Krause update rule:
            if self.update_rule == "HK":
                next_opinions[i] = o_eps

            elif self.update_rule=="smooth-HK":

                for ok in neigh_ops:
                    normaliz=0
                    for ol in neigh_ops:
                        normaliz += (1 + tanh(self.kappa*self.eps - self.kappa*np.abs(ol - oi)))
                    
                    next_opinions[i] += ok*(1 + tanh(self.kappa*self.eps - self.kappa*np.abs(ok - oi)))/normaliz

            # Assign next opinions for a SABER update rule:
            elif self.update_rule == "SABER":
  
                if self.static_traits==True:
                    a = self.traits[0][i,0]   # agreeableness
                    r = self.traits[0][i,1]   # receptiveness
                    n = self.traits[0][i,2]   # neuroticism
                else:
                    a = self.traits[timestep][i,0]   # agreeableness
                    r = self.traits[timestep][i,1]   # receptiveness
                    n = self.traits[timestep][i,2]   # neuroticism

                # also consider those that are more than delta away:
                delta_ops = np.abs(all_ops - oi) <= self.delta
                delta_neigh = np.ma.array(neigh_ops, mask=delta_ops).filled(fill_value=0.0)
                delta_weights = np.ma.array(all_neigh, mask=delta_ops).filled(fill_value=0.0)
                # get average value of the opinions that are more than delta away, 
                # weighted by the bonds of the corresponding neighbors:
                if sum(delta_weights)==0:
                    o_delta = oi
                else:
                    o_delta = delta_weights@delta_neigh/sum(delta_weights) 

                # draw random number to compare to a:
                random_num = ra.uniform(0.0,1.0)
                # assign next value of the opinion:
                if random_num <= a:
                    next_opinions[i] = (1-n)*((1 - r)*oi + r*o_eps) \
                                    + n*(oi + np.sign(oi - o_delta)*np.amin([np.abs(oi - np.sign(oi - o_delta)), np.abs(oi - o_delta)]) )
                else:
                    next_opinions[i] = oi

                if self.verbose==True:
                    print(f"epsilon is {self.eps}")
                    print(f"Mask for eps-neighbors: {eps_ops}")
                    print(f"opinions of eps-neighbors: {eps_neigh}")
                    print(f"Weights of eps-neighbors: {eps_weights}")
                    print(f"Value of o_eps: {o_eps}")
                    print(f"\n")
                    print(f"delta is {self.delta}")
                    #print(f"np.abs(all_ops - o): {np.abs(all_ops - o)}")
                    print(f"Mask for delta-neighbors: {delta_ops}")
                    print(f"opinions of delta-neighbors: {delta_neigh}")
                    print(f"Weights of delta-neighbors: {delta_weights}")
                    print(f"Value of o_delta: {o_delta}")
                    print(f"Next value of opinion i: {next_opinions[i]}")
                    print(f"\n")
                    print(f"agreeableness is {a}")

                    
                # # Manually enforce left/right boundaries:
                # if next_opinions[i,0] > 1.0:
                #     next_opinions[i,0] = 1.0

                # if next_opinions[i,0] < 0.0:
                #     next_opinions[i,0] = 0.0


        if self.static_traits==False:

            # TODO: modify character traits in time.
            # copy a, r, and n attributes (character traits are not modified)
            next_traits[:,0] = self.traits[timestep][:,0] 
            next_traits[:,1] = self.traits[timestep][:,1] 
            next_traits[:,2] = self.traits[timestep][:,2] 


        return next_opinions, next_traits
    ######################################################################################



    ######################################################################################
    def time_evolution_opinions(self, 
                                realization, 
                                plot=False,
                                load=False,
                                **kwargs):
        
        # Update t_final from the input if given
        t_final_input = kwargs.get("t_final", None)
        eps_input = kwargs.get("eps", None)
        verbose_input = kwargs.get("verbose", None)
        if t_final_input is not None:
            self.t_final = t_final_input
        if eps_input is not None:
            self.eps = eps_input
        if verbose_input is not None:
            self.verbose = verbose_input

        if self.N_edges is None:
            self.N_edges=0

        loaded=False
        # try to find already saved data:
        if load==True:
            try:
                data = np.load("data/time-evolution-opinions-N_actors-{0}-initial_distr-{1}-graph_type-{2}-N-edges-{3}-bond_distr-{4}-bond_type-{5}-eps-{6}-delta-{7}-a_min-{8}-a_max-{9}-r-{10}-{11}-n-{12}-{13}-t_final-{14}-realization-{15}.npy".format(self.N_actors, self.initial_distr, self.graph_type, self.N_edges, self.bond_distr, self.bond_type, np.round(self.eps,4), self.delta, self.a_min, self.a_max, self.r_min, self.r_max, self.n_min, self.n_max, self.t_final, realization), allow_pickle=True, encoding='latin1')
                opinions_t = data[()]["opinions_t"]
                if self.verbose==True:
                    print("Previous data found! Loading...")
                loaded=True

            except:
                print("No data found. Calculating...")
                loaded=False

        if load==False or loaded==False:
            # Create list where to save data:
            opinions_t = []
            opinions_t.append(self.opinions[0])

            # Time evolve:
            if self.t_final is not None:
                for t in range(self.t_final):

                    if self.verbose==True:
                        print("\n")
                        print(f"Updating time step {t+1}")
                        for N in range(self.N_actors):
                            plt.plot(np.linspace(0, t, t+1), np.array(opinions_t)[:,N], lw=3, label=r"{0}".format(N))
                        plt.xlim([0,t])
                        plt.xlabel(r"time step $t$")
                        plt.ylabel(r"opinion $o_i$")
                        plt.legend(loc='best')
                        plt.show()
                        plt.close()

                    opinions_next, traits_next = self.time_step_opinions_and_traits(timestep=t)
    
                    # TODO: decide whether to keep opinions_t or just update attribute self.opinions after every time step.
                    opinions_t.append(opinions_next)
                    self.opinions.append(opinions_next)
                    if self.static_traits==False:
                        self.traits.append(traits_next)

            # Automatically stop the time evolution after convergence:
            elif self.t_final is None:

                opinion_diff = 6666666

                t=0
                while opinion_diff > self.convergence_thresh:
                
                    if self.verbose==True:
                        print(f"Updating time step {t}")
                        print(f"Opinion difference is {opinion_diff}")
                        for N in range(self.N_actors):
                            plt.plot(np.linspace(0, t, t+1), np.array(opinions_t)[:,N], lw=3, label=r"{0}".format(N))
                        plt.xlim([0,t])
                        plt.legend(loc='best')
                        plt.show()
                        plt.close()
                    
                    opinions_next, traits_next = self.time_step_opinions_and_traits(timestep=t)
                    self.opinions.append(opinions_next)
                    opinions_t.append(opinions_next)
                    if self.static_traits==False:
                        self.traits.append(traits_next)

                    # Minimal convergence time is Omega(N_actors**2) according to literature:
                    if t>1:
                        opinion_diff = np.linalg.norm(np.array(opinions_t)[t,:] - np.array(opinions_t)[t-1,:])
                        if self.verbose==True:
                            print(f"Opinion difference is {opinion_diff}")
                    # set maximum time evolution:
                    if t>100000:
                        opinion_diff = 0

                    t+=1

            # Save data:
            data = {"opinions_t": opinions_t}
            # datetime object containing current date and time
            # now = dt.datetime.now()
            # dt_string = now.strftime("%d-%m-%Y-%H-%M-%S")
            np.save("data/time-evolution-opinions-N_actors-{0}-initial_distr-{1}-graph_type-{2}-N-edges-{3}-bond_distr-{4}-bond_type-{5}-eps-{6}-delta-{7}-a_min-{8}-a_max-{9}-r-{10}-{11}-n-{12}-{13}-t_final-{14}-realization-{15}.npy".format(self.N_actors, self.initial_distr, self.graph_type, self.N_edges, self.bond_distr, self.bond_type, np.round(self.eps,4), self.delta, self.a_min, self.a_max, self.r_min, self.r_max, self.n_min, self.n_max, self.t_final, realization), data)

        t_final_empirical = len(np.array(opinions_t)[:,0])
        if plot==True:
            colors = plt.cm.rainbow(np.linspace(1,0,self.N_actors))
            for N in range(self.N_actors):
                plt.plot(np.linspace(0, t_final_empirical-1, t_final_empirical), np.array(opinions_t)[:,N], lw=3, label=r"{0}".format(N), color=colors[N])
            plt.xlim([0,t_final_empirical])
            plt.ylim([-0.1,1.1])
            plt.xlabel(r"time step $t$")
            plt.ylabel(r"opinion $o_i$")
            #plt.legend(loc='best', ncol=3)
            plt.show()
            plt.close()

        if self.verbose==True:
            print(f"Final time step in time evolution: {t_final_empirical}")

        return np.array(opinions_t)
    ######################################################################################


    ######################################################################################
    def Jacobian_HK(self, 
                    timestep,
                    plot=False):
        

        # Initialize Jacobian:
        J = np.zeros(shape=(self.N_actors,self.N_actors))

        for oi, i in enumerate(self.opinions[:,timestep]):
            neigh_num_i = utils.get_number_of_neighbors(opinions=self.opinions[:,timestep],eps=self.eps,i=i,kappa=self.kappa,smoothening=True)
            #print(f"oi: {oi}")

            for oj, j in enumerate(self.opinions[:,timestep]):

                p = 0
                for ok, k in enumerate(self.opinions[:,timestep]):
                    p += (ok-oj)*(1 + tanh(self.kappa*self.eps - self.kappa*np.abs(ok-oi)) )

                J[i,j] = (1 + tanh(self.kappa*self.eps - self.kappa*np.abs(oj - oi)))/neigh_num_i + (1 - tanh(self.kappa*self.eps - self.kappa*np.abs(oj-oi))**2)*sign(oj-oi)*p/neigh_num_i**2

        if plot==True:
            utils.plot_Jacobian_HK(J, timestep)

        return J
    ######################################################################################


    ######################################################################################
    def Lyapunov_exponents_HK(N_actors, initial_distr, eps, kappa, t_final, realization, plot_Phi=False):
        """
        
        Notes:
        ------
        For discrete dynamical maps, the Lyapunov exponent is defined as 

        l_i = lim_{m \to \infty } 1/2m*(ln|EV[Phi_m^T Phi_m]|)

        where Phi_m = f'(x_{m-1}) f'(x_{m-2}) ... f'(x_0)
        and f' is the Jacobian of the dynamical map x_{k+1} = f(x_k)

        """
        all_opinions = time_evolution_opinions(N_actors=N_actors,
                                            initial_distr=initial_distr,
                                            graph_type="HK",
                                            bond_distr="ones",
                                            bond_type="directed",
                                            eps=eps,
                                            delta=0,
                                            a_min=1.0,
                                            a_max=1.0,
                                            r_min=1.0,
                                            r_max=1.0,
                                            n_min=0.0,
                                            n_max=0.0,
                                            t_final=t_final,
                                            realization=realization, 
                                            convergence_thresh=1e-12, 
                                            verbose=False, 
                                            plot=False,
                                            load=False)
        opinions_t = all_opinions[:,:,0]

        if t_final is None:
            t_final = len(opinions_t[:,0])
            #print(f"t final is {t_final}")

        #print(f"opinions at {t_final}: {opinions_t[t_final-1,:]}")

        # Calculate product of Jacobians Phi:
        Phi = Jacobian_HK(opinions=opinions_t[0,:], eps=eps, kappa=kappa, time=0, verbose=False, plot=False)
        for t in range(1,t_final):
            Phi = Jacobian_HK(opinions=opinions_t[t,:], eps=eps, kappa=kappa, time=t, verbose=False, plot=False)@Phi
            #print(f"Phi at {t} is {Phi}")

        if plot_Phi==True:
            pltmat = plt.imshow(Phi)
            plt.colorbar(pltmat)
            plt.title(r"$\Phi_m$")
            plt.show()
            plt.close()

            pltmat = plt.imshow(Phi.T@Phi)
            plt.colorbar(pltmat)
            plt.title(r"$\Phi_m^T \Phi_m$")
            plt.show()
            plt.close()


        # Getting the eigenvalues of the product of Jacobians:
        EVals, _ = spla.eig(Phi.T@Phi)  
        li = 1/(2*t_final)*np.log(np.abs(EVals))

        return li

    ######################################################################################




    ######################################################################################
    def steady_state_averages(self,
                              realizations,
                              plot=False,
                              load=False,
                              **kwargs):

        loaded=False
        if load==True:
            try:
                # Load data:
                data = np.load("data/ss-averages-N_actors-{0}-initial_distr-{1}-graph_type-{2}-bond_distr-{3}-bond_type-{4}-eps-{5}-delta-{6}-a_min-{7}-a_max-{8}-r-{9}-{10}-n-{11}-{12}-t_final-{13}-realizations-{14}.npy".format(self.N_actors, self.initial_distr, self.graph_type, self.bond_distr, self.bond_type, np.round(self.eps,4), self.delta, self.a_min, self.a_max, self.r_min, self.r_max, self.n_min, self.n_max, self.t_final, realizations), allow_pickle=True, encoding='latin1')
                print("Average steady state data found! Loading...")
                avg_op_avg = data[()]["avg_op_avg"]
                clusters_num_avg = data[()]["clusters_num_avg"]
                avg_conv_time = data[()]["avg_conv_time"]

                loaded=True

            except:
                print("No average steady state data found. Calculating...")

        if load==False or loaded==False:
            avg_op_list = []
            clusters_list = []
            cluster_avg_list = []
            avg_conv_time_list = []


        for draw in range(realizations):

            #print("\n=====================================")
            print(f"Calculating realization number {draw}.")
            #print("=====================================\n")

            self.reset_dynamics()
            opinions_t = self.time_evolution_opinions(realization=draw,
                                                      plot=False,
                                                      load=load,
                                                      **kwargs)
            opinions_ss = opinions_t[-1,:]
            convergence_time = len(np.array(opinions_t)[:,0])

            # average opinion:
            avg_op = np.sum(opinions_ss)/len(opinions_ss)
            avg_op_list.append(avg_op)
            avg_conv_time_list.append(convergence_time)

            # Number of clusters and their average (centroids) via k-means:
            #clusters, cluster_avgs = find_clusters(opinions_ss, "MeanShift")
            clusters, cluster_avgs = utils.find_clusters(opinions_ss, "Simple")
            clusters_list.append(clusters)
            cluster_avg_list.append(cluster_avgs)

            print(f"cluster list: {clusters_list}")
            print(f"cluster averages list: {cluster_avg_list}")

        if plot==True:
            fig = plt.figure()

            ax1 = fig.add_subplot(311)
            ax1.scatter(np.linspace(0, realizations-1, realizations), np.array(clusters_list), lw=3, color='blue')
            #ax1.set_xlabel("Realizations")
            ax1.set_ylabel("Clusters")

            ax2 = fig.add_subplot(312)
            ax2.scatter(np.linspace(0, realizations-1, realizations), np.array(avg_op_list), lw=3, color='red')
            #ax2.set_xlabel("Realizations")
            ax2.set_ylabel("Average opinion")

            ax3 = fig.add_subplot(313)
            for xe, ye in zip(np.linspace(0, realizations-1, realizations), cluster_avg_list):
                if type(ye)==np.float64:
                    l=1
                else:
                    l=len(ye)
                ax3.scatter([xe] * l, ye)
            ax3.set_xlabel("Realizations")
            ax3.set_ylabel("Cluster centroids")

            plt.tight_layout()
            plt.show()
            plt.close()

        avg_op_avg = np.sum(avg_op_list)/len(avg_op_list)
        clusters_num_avg = np.sum(clusters_list)/len(clusters_list)
        avg_conv_time = np.sum(avg_conv_time_list)/len(avg_conv_time_list)

        # Save data:
        data = {"avg_op_avg": avg_op_avg, "clusters_num_avg": clusters_num_avg, "avg_conv_time": avg_conv_time}
        np.save("data/ss-averages-N_actors-{0}-initial_distr-{1}-graph_type-{2}-bond_distr-{3}-bond_type-{4}-eps-{5}-delta-{6}-a_min-{7}-a_max-{8}-r-{9}-{10}-n-{11}-{12}-t_final-{13}-realizations-{14}.npy".format(self.N_actors, self.initial_distr, self.graph_type, self.bond_distr, self.bond_type, np.round(self.eps,4), self.delta, self.a_min, self.a_max, self.r_min, self.r_max, self.n_min, self.n_max, self.t_final, realizations), data)

        return avg_op_avg, clusters_num_avg, avg_conv_time

    ######################################################################################




################################
######       MOVIES      ######
################################
######################################################################################

######################################################################################
def movie_time_evolution(N_actors, initial_distr, graph_type, bond_distr, bond_type, eps_array, delta, a_min, a_max, r_min, r_max, n_min, n_max, t_final, load=False):

    colors = plt.cm.rainbow(np.linspace(0,1,N_actors))

    if load==False:
        # Get data:
        opinions_t_list = []
        for eps in eps_array:
            opinions_t = time_evolution_opinions(N_actors=N_actors, 
                                                initial_distr=initial_distr, 
                                                graph_type=graph_type, 
                                                bond_distr=bond_distr, 
                                                bond_type=bond_type, 
                                                eps=eps, 
                                                delta=delta, 
                                                a_min=a_min, 
                                                a_max=a_max, 
                                                r_min=r_min, 
                                                r_max=r_max, 
                                                n_min=n_min, 
                                                n_max=n_max, 
                                                t_final=t_final, 
                                                convergence_thresh=1e-12, 
                                                verbose=False, 
                                                plot=False)
    
    # First data point:
    data = np.load("data/time-evolution-opinions-N_actors-{0}-initial_distr-{1}-graph_type-{2}-bond_distr-{3}-bond_type-{4}-eps-{5}-delta-{6}-a_min-{7}-a_max-{8}-r-{9}-{10}-n-{11}-{12}-t_final-{13}.npy".format(N_actors, initial_distr, graph_type, bond_distr, bond_type, np.round(eps_array[0],4), delta, a_min, a_max, r_min, r_max, n_min, n_max, t_final), allow_pickle=True, encoding='latin1')
    opinions_t = data[()]["opinions_t"]

    ##############
    # movie part #
    ##############

    # Set up formatting for the movie files
    Writer = animation.writers['ffmpeg']
    writer = Writer(fps=15, metadata=dict(artist='PaoloMolignini'), bitrate=1800)
        
    # Create figure
    fig = plt.figure()
    ax1 = plt.gca()
    for N in range(N_actors):
            ax1.plot(np.linspace(0, t_final, t_final+1), np.array(opinions_t)[:,N,0], lw=3, label=r"{0}".format(N), color=colors[N])  
    ax1.set_xlim([0,t_final])
    ax1.set_ylim([-0.1,1.1])
    ax1.set_xlabel("Time step")
    ax1.set_ylabel("Opinions")
    ax1.set_title(r"Time evolution for $\epsilon$=%s"%(eps_array[0]))

    # Total number of frames:
    frame_number = len(eps_array)
    # Defines the frame speed of the movie (interval between frames in ms):
    speed_movie = 200
    #values of the function to be updated:
    fargs = [fig, ax1, N_actors, initial_distr, graph_type, bond_distr, bond_type, eps_array, delta, a_min, a_max, r_min, r_max, n_min, n_max, t_final, colors]
            
    # Animation iteration routine:
    ani = animation.FuncAnimation(fig, animate_time_evol, frames=np.arange(1,frame_number), fargs=[fargs], interval=speed_movie, blit=False, repeat=False)
        
    ani.save('movies/time-evol-N_actors-{0}-initial_distr-{1}-graph_type-{2}-bond_distr-{3}-bond_type-{4}-eps-{5}-{6}-{7}-delta-{8}-a_min-{9}-a_max-{10}-r-{11}-{12}-n-{13}-{14}-t_final-{15}.mp4'.format(N_actors, initial_distr, graph_type, bond_distr, bond_type, eps_array[0], eps_array[-1], len(eps_array), delta, a_min, a_max, r_min, r_max, n_min, n_max, t_final), writer=writer, dpi=500)

    plt.close()
    return

###########################################################################################
#callable function for the iteration of the frames in the animation:
def animate_time_evol(frames, fargs):
    
    # Get fargs:
    fig = fargs[0]
    ax1 = fargs[1]
    N_actors = fargs[2]
    initial_distr = fargs[3]
    graph_type = fargs[4]
    bond_distr = fargs[5]
    bond_type = fargs[6]
    eps_array = fargs[7]
    eps = eps_array[frames]
    delta = fargs[8]
    a_min = fargs[9]
    a_max = fargs[10]
    r_min = fargs[11]
    r_max = fargs[12]
    n_min = fargs[13]
    n_max = fargs[14]
    t_final = fargs[15]
    colors = fargs[16]
    
    # Current opinions:
    print(f"Working on frame: {frames}")
    
    data = np.load("data/time-evolution-opinions-N_actors-{0}-initial_distr-{1}-graph_type-{2}-bond_distr-{3}-bond_type-{4}-eps-{5}-delta-{6}-a_min-{7}-a_max-{8}-r-{9}-{10}-n-{11}-{12}-t_final-{13}.npy".format(N_actors, initial_distr, graph_type, bond_distr, bond_type, np.round(eps,4), delta, a_min, a_max, r_min, r_max, n_min, n_max, t_final), allow_pickle=True, encoding='latin1')
    opinions_t = data[()]["opinions_t"]

    # Clear canvas and plot current data:
    ax1.cla()
    for N in range(N_actors):
            ax1.plot(np.linspace(0, t_final, t_final+1), np.array(opinions_t)[:,N,0], lw=3, label=r"{0}".format(N), color=colors[N])  
    ax1.set_xlim([0,t_final])
    ax1.set_ylim([-0.1,1.1])
    ax1.set_xlabel("Time step")
    ax1.set_ylabel("Opinions")
    ax1.set_title(r"Time evolution for $\epsilon$=%s"%(eps_array[frames]))

    return ax1,
###########################################################################################




######################################################################################
def compute_eps_dependence_HK(N_actors,
                              realizations,
                              eps_array,
                              plot=False,
                              load=False,
                              **kwargs):

    a_min = kwargs.get("a_min") 
    a_max = kwargs.get("a_max")
    r_min = kwargs.get("r_min")
    r_max = kwargs.get("r_max")
    n_min = kwargs.get("n_min")
    n_max = kwargs.get("n_max")
    delta = kwargs.get("delta")
    graph_type = kwargs.get("graph_type")
    bond_type = kwargs.get("bond_type")
    bond_distr = kwargs.get("bond_distr")
    initial_distr = kwargs.get("initial_distr")
    col_stoch = kwargs.get("col_stoch")
    N_edges = kwargs.get("N_edges")
    verbose = kwargs.get("verbose")
    update_rule = kwargs.get("update_rule")
    plot_flag = kwargs.get("plot_flag")
    loops = kwargs.get("loops")
    static_graph = kwargs.get("static_graph")
    static_traits = kwargs.get("static_traits")

    eps_points = len(eps_array)
    avg_cluster_num_arr = np.zeros(shape=(eps_points))
    avg_conv_time_arr = np.zeros(shape=(eps_points))

    for idx, eps in enumerate(eps_array):

        print("\n=====================================================")
        print(f"Calculating steady state properties for epsilon={eps}.")
        print("=====================================================\n")

        OD = OpinionDynamics(N_actors=N_actors, 
                            a_min=a_min, 
                            a_max=a_max, 
                            r_min=r_min, 
                            r_max=r_max, 
                            n_min=n_min, 
                            n_max=n_max, 
                            eps=eps, 
                            delta=delta, 
                            graph_type=graph_type,
                            bond_type=bond_type, 
                            bond_distr=bond_distr, 
                            initial_distr=initial_distr, 
                            col_stoch=col_stoch, 
                            N_edges=N_edges, 
                            verbose=verbose, 
                            update_rule=update_rule, 
                            plot_flag=plot_flag, 
                            loops=loops,
                            static_graph=static_graph,
                            static_traits=static_traits)

        _, cluster_avg, avg_conv_time = OD.steady_state_averages(realizations=realizations,
                                                                 plot=plot,
                                                                 load=load,
                                                                 **kwargs)

        avg_cluster_num_arr[idx] = cluster_avg
        avg_conv_time_arr[idx] = avg_conv_time

    data = {"cluster_num": avg_cluster_num_arr, "avg_conv_time": avg_conv_time_arr}
    np.save("data/HK-phase-diagram-realizations-{0}-N_actors-{1}-graph_type-{2}-eps-{3}-{4}-{5}.npy".format(realizations, N_actors, OD.graph_type, eps_array[0], eps_array[-1], eps_points), data)

    # Plotting:
    fig = plt.figure(figsize=(9,6))
    ax11 = fig.add_subplot(111)
    ax11right = ax11.twinx()

    clusterplot = ax11.plot(eps_array, avg_cluster_num_arr, lw=3, color='blue', label="Number of clusters")
    timeplot = ax11right.plot(eps_array, avg_conv_time_arr, lw=3, color='red', label="Convergence time")
    ax11.set_xlabel(r'$\epsilon$')
    ax11.set_ylabel(r'Average number of clusters')
    ax11right.set_ylabel(r'Convergence time')

    # legends:
    lns = clusterplot+timeplot
    labs = [l.get_label() for l in lns]
    ax11.legend(lns, labs, loc='best', prop={'size': 12}, framealpha=0.9, handlelength=2.5, ncol=1)

    plt.tight_layout()
    plt.show()
    plt.close()

    return avg_cluster_num_arr
######################################################################################



######################################################################################
def compute_phase_diagram_HK(realizations,
                             N_array,
                             eps_array,
                             load=False,
                             **kwargs):

    a_min = kwargs.get("a_min") 
    a_max = kwargs.get("a_max")
    r_min = kwargs.get("r_min")
    r_max = kwargs.get("r_max")
    n_min = kwargs.get("n_min")
    n_max = kwargs.get("n_max")
    delta = kwargs.get("delta")
    graph_type = kwargs.get("graph_type")
    bond_type = kwargs.get("bond_type")
    bond_distr = kwargs.get("bond_distr")
    initial_distr = kwargs.get("initial_distr")
    col_stoch = kwargs.get("col_stoch")
    N_edges = kwargs.get("N_edges")
    verbose = kwargs.get("verbose")
    update_rule = kwargs.get("update_rule")
    plot_flag = kwargs.get("plot_flag")
    loops = kwargs.get("loops")
    static_graph = kwargs.get("static_graph")
    static_traits = kwargs.get("static_traits")

    N_points = len(N_array)
    eps_points = len(eps_array)
    avg_cluster_num_arr = np.zeros(shape=(N_points,eps_points))
    avg_conv_time_arr = np.zeros(shape=(N_points,eps_points))

    for idx_N, N_actors in enumerate(N_array):
        for idx_eps, eps in enumerate(eps_array):

            print("\n======================================================================")
            print(f"Calculating steady state properties for N={N_actors} and epsilon={eps}.")
            print("========================================================================\n")

            OD = OpinionDynamics(N_actors=N_actors, 
                                a_min=a_min, 
                                a_max=a_max, 
                                r_min=r_min, 
                                r_max=r_max, 
                                n_min=n_min, 
                                n_max=n_max, 
                                eps=eps, 
                                delta=delta, 
                                graph_type=graph_type,
                                bond_type=bond_type, 
                                bond_distr=bond_distr, 
                                initial_distr=initial_distr, 
                                col_stoch=col_stoch, 
                                N_edges=N_edges, 
                                verbose=verbose, 
                                update_rule=update_rule, 
                                plot_flag=plot_flag, 
                                loops=loops,
                                static_graph=static_graph,
                                static_traits=static_traits)
            
            _, cluster_avg, avg_conv_time = OD.steady_state_averages(realizations=realizations,
                                                                     plot=False,
                                                                     load=load,
                                                                     **kwargs)

            avg_cluster_num_arr[idx_N,idx_eps] = cluster_avg
            avg_conv_time_arr[idx_N,idx_eps] = avg_conv_time

        data = {"cluster_num": avg_cluster_num_arr[idx_N,:], "avg_conv_time": avg_conv_time_arr[idx_N,:]}
        np.save("data/HK-phase-diagram-realizations-{0}-N_actors-{1}-graph_type-{2}-eps-{3}-{4}-{5}.npy".format(realizations, N_actors, OD.graph_type, eps_array[0], eps_array[-1], eps_points), data)


    fig = plt.figure(figsize=(8,6))
    X, Y = np.meshgrid(eps_array, N_array)

    ax11 = fig.add_subplot(211)
    clusterplot = ax11.pcolormesh(X, Y, avg_cluster_num_arr, cmap="viridis")
    ax11.set_xlabel(r'$\epsilon$')
    ax11.set_ylabel(r'$N$')
    ax11.set_title(r'Average number of clusters')
    plt.colorbar(clusterplot)

    ax21 = fig.add_subplot(212)
    timeplot = ax21.pcolormesh(X, Y, avg_conv_time_arr, cmap="inferno")
    ax21.set_xlabel(r'$\epsilon$')
    ax21.set_ylabel(r'$N$')
    ax21.set_title(r'Convergence time')
    plt.colorbar(timeplot)


    # legends:
    plt.tight_layout()
    plt.show()
    plt.close()

    return avg_cluster_num_arr, avg_conv_time_arr
######################################################################################




######################################################################################
def plot_Lyapunov_exp_eps_HK(self,
                                eps_array,
                                realization):
    """
    """

    Lyapunov_list = []

    for eps_idx, eps in enumerate(eps_array):
        print(f"Working on eps={eps}")
        Lyapunov_exps = Lyapunov_exponents_HK(N_actors, initial_distr, eps, kappa, t_final, realization)
        print(f"Lyapunov exponents: {Lyapunov_exps}")
        Lyapunov_list.append(np.amax(np.real(Lyapunov_exps)))

    Lyapunov_arr = np.array(Lyapunov_list)

    fig = plt.figure(figsize=(8,6))
    ax = fig.add_subplot(111)

    plot = ax.scatter(eps_array, Lyapunov_arr)
    ax.hlines(xmin=eps_array[0], xmax=eps_array[-1], y=0.0, color='red', lw=2)

    ax.set_xlabel(r"$\epsilon$")
    ax.set_ylabel(r"$\mathrm{max}_i \lambda_i$")
    ax.set_title(r"Maximal Lyapunov exponent as a function of $\epsilon$")

    plt.tight_layout()
    plt.show()
    plt.close()

    return
######################################################################################


######################################################################################
def plot_convergence_eps(N_actors,
                         eps_array,
                         realization,
                         **kwargs):
    
    a_min = kwargs.get("a_min") 
    a_max = kwargs.get("a_max")
    r_min = kwargs.get("r_min")
    r_max = kwargs.get("r_max")
    n_min = kwargs.get("n_min")
    n_max = kwargs.get("n_max")
    delta = kwargs.get("delta")
    graph_type = kwargs.get("graph_type")
    bond_type = kwargs.get("bond_type")
    bond_distr = kwargs.get("bond_distr")
    initial_distr = kwargs.get("initial_distr")
    col_stoch = kwargs.get("col_stoch")
    N_edges = kwargs.get("N_edges")
    verbose = kwargs.get("verbose")
    update_rule = kwargs.get("update_rule")
    plot_flag = kwargs.get("plot_flag")
    loops = kwargs.get("loops")
    static_graph = kwargs.get("static_graph")
    static_traits = kwargs.get("static_traits")

    conv_t = []
    cluster_num = []
    for eps in eps_array:

        print("\n=====================================================")
        print(f"Calculating steady state properties for epsilon={eps}.")
        print("=====================================================\n")
    
        OD = OpinionDynamics(N_actors=N_actors, 
                    a_min=a_min, 
                    a_max=a_max, 
                    r_min=r_min, 
                    r_max=r_max, 
                    n_min=n_min, 
                    n_max=n_max, 
                    eps=eps, 
                    delta=delta, 
                    graph_type=graph_type,
                    bond_type=bond_type, 
                    bond_distr=bond_distr, 
                    initial_distr=initial_distr, 
                    col_stoch=col_stoch, 
                    N_edges=N_edges, 
                    verbose=verbose, 
                    update_rule=update_rule, 
                    plot_flag=plot_flag, 
                    loops=loops,
                    static_graph=static_graph,
                    static_traits=static_traits
                    )
                    
        opinions_t = OD.time_evolution_opinions(realization=realization, plot=False, eps=eps, **kwargs)
        # Get convergence time:
        t_final = len(np.array(opinions_t)[:,0])
        conv_t.append(t_final)

        # Get number of clusters:
        clusters, _ = utils.find_clusters(opinions_t[-1,:], "MeanShift")
        cluster_num.append(clusters)

    conv_t = np.array(conv_t)
    cluster_num = np.array(cluster_num)

    # Plotting:
    fig = plt.figure(figsize=(8,6))
    ax = fig.add_subplot(111)
    ax_right = ax.twinx()

    conv_plot = ax.plot(eps_array, conv_t, lw=3, color='blue', label=r"Convergence time")
    clust_plot = ax_right.plot(eps_array, cluster_num, lw=3, color='red', label=r"Cluster number")

    ax.set_xlabel(r"$\epsilon$")
    ax.set_ylabel(r"Convergence time")
    ax_right.set_ylabel(r"Cluster number")

    # legends:
    lns = conv_plot+clust_plot
    labs = [l.get_label() for l in lns]
    ax.legend(lns, labs, loc='best', prop={'size': 12}, framealpha=0.9, handlelength=2.5, ncol=1)

    plt.tight_layout()
    plt.show()
    plt.close()

    return

######################################################################################




######################################################################################
def avg_clusters_vs_neighbors_HK(N_actors,
                                 N_edge_increment,
                                 realizations,
                                 eps,
                                 plot_ss,
                                 **kwargs,
                                 ):
    min_edges = kwargs.get("min_edges", None)
    max_edges = kwargs.get("max_edges", None)

    if min_edges is None or min_edges < N_actors-1:
        min_edges = N_actors - 1
    if max_edges is None or max_edges>int(N_actors*(N_actors-1)/2):
        max_edges =  int(N_actors*(N_actors-1)/2)
    N_edges_arr = np.arange(min_edges, max_edges+1, N_edge_increment)

    cluster_arr = np.zeros(shape=(len(N_edges_arr), realizations))
    for N_edges_idx, N_edges in enumerate(N_edges_arr):

        # Initialize OpinionDynamics object
        OD = OpinionDynamics(N_actors=N_actors,
                    eps=eps, 
                    graph_type="simply-connected-random",
                    bond_type="undirected", 
                    bond_distr="ones", 
                    initial_distr="uniform", 
                    col_stoch=False, 
                    N_edges=N_edges, 
                    verbose=False, 
                    update_rule="HK", 
                    loops=True,
                    static_graph=True,
                    static_traits=True,
                    **kwargs
                    )
        
        for realization in range(realizations):

            if realization%200==0:
                print("\n=====================================================")
                print(f"Calculating dynamics for N_edges={N_edges} and realization={realization}.")
                print("=====================================================\n")

            opinions_t = OD.time_evolution_opinions(realization=realization, plot=plot_ss, eps=eps, load=True, **kwargs)

            # Get convergence time:
            #t_final = len(np.array(opinions_t)[:,0])
            #conv_t.append(t_final)

            # Get number of clusters:
            clusters, _ = utils.find_clusters(opinions_t[-1,:], "Simple")
            cluster_arr[N_edges_idx,realization] = clusters
            
            OD.reset_graph()
            OD.reset_dynamics()

        cluster_arr[N_edges_idx, :] = np.sort(cluster_arr[N_edges_idx,:])


    # Plotting:
    fig = plt.figure(figsize=(8,6))
    ax = fig.add_subplot(111)

    X, Y = np.meshgrid(N_edges_arr/N_actors, np.linspace(0.0,1.0,realizations))
    plot = ax.pcolormesh(X, Y, cluster_arr.T, cmap="inferno", vmin=1, vmax=5)
    plt.colorbar(plot, label=r"Cluster number")

    ax.set_xlabel(r"Average edge for node")
    ax.set_ylabel(r"Cluster occurrence")
    ax.set_title(r"Steady state clusters in Hegselmann-Krause model for %s agents and $\epsilon=$%s"%(N_actors, eps))
    plt.tight_layout()
    plt.show()
    plt.close()
    
    return




######################################################################################




######################################################################################
def avg_clusters_vs_eps_simply_connected_HK(N_actors,
                                            N_edges,
                                            realizations,
                                            eps_array,
                                            plot_ss,
                                            **kwargs,
                                            ):
   
    cluster_arr = np.zeros(shape=(len(eps_array), realizations))
    cluster_avg_arr = np.zeros(shape=(len(eps_array)))
    for eps_idx, eps in enumerate(eps_array):

        # Initialize OpinionDynamics object
        OD = OpinionDynamics(N_actors=N_actors,
                    eps=eps, 
                    graph_type="simply-connected-random",
                    bond_type="undirected", 
                    bond_distr="ones", 
                    initial_distr="uniform", 
                    col_stoch=False, 
                    N_edges=N_edges, 
                    verbose=False, 
                    update_rule="HK", 
                    loops=True,
                    static_graph=True,
                    static_traits=True,
                    **kwargs
                    )
        avg = 0
        for realization in range(realizations):

            if realization%200==0:
                print("\n====================================================================")
                print(f"Calculating dynamics for epsilon={eps} and realization={realization}.")
                print("====================================================================\n")

            opinions_t = OD.time_evolution_opinions(realization=realization, plot=plot_ss, eps=eps, load=True, **kwargs)

            # Get convergence time:
            #t_final = len(np.array(opinions_t)[:,0])
            #conv_t.append(t_final)

            # Get number of clusters:
            clusters, _ = utils.find_clusters(opinions_t[-1,:], "Simple")
            cluster_arr[eps_idx,realization] = clusters
            avg += clusters/realizations
            OD.reset_graph()
            OD.reset_dynamics()

        cluster_avg_arr[eps_idx] = avg
        cluster_arr[eps_idx, :] = np.sort(cluster_arr[eps_idx,:])


    # Plotting:
    fig = plt.figure(figsize=(8,6))
    ax = fig.add_subplot(211)

    X, Y = np.meshgrid(eps_array, np.linspace(0.0,1.0,realizations))
    plot = ax.pcolormesh(X, Y, cluster_arr.T, cmap="inferno", vmin=1, vmax=5)
    plt.colorbar(plot, label=r"Cluster number")

    ax.set_xlabel(r"$\epsilon$")
    ax.set_ylabel(r"Cluster occurrence")
    ax.set_title(r"Steady state clusters in Hegselmann-Krause model for %s agents and $m=$%s bonds"%(N_actors, N_edges))

    ax2 = fig.add_subplot(212)
    plot = ax2.plot(eps_array, cluster_avg_arr, lw=3, color='blue')
    ax2.set_xlabel(r"$\epsilon$")
    ax2.set_ylabel(r"Average cluster number")
    ax2.set_title(r"Steady state clusters in Hegselmann-Krause model for %s agents and $m=$%s bonds"%(N_actors, N_edges))
    plt.tight_layout()
    plt.show()
    plt.close()    

    return




######################################################################################



######################################################################################
def avg_clusters_vs_eps_and_edges_simply_connected_HK(N_actors,
                                                      N_edges_array,
                                                      realizations,
                                                      eps_array,
                                                      plot_ss,
                                                      **kwargs,
                                                      ):
   
    cluster_avg_arr = np.zeros(shape=(len(eps_array), len(N_edges_array)))
    for eps_idx, eps in enumerate(eps_array):
        for N_edges_idx,N_edges in enumerate(N_edges_array):

            # Initialize OpinionDynamics object
            OD = OpinionDynamics(N_actors=N_actors,
                                 eps=eps,
                                 graph_type="simply-connected-random",
                                 bond_type="undirected",
                                 bond_distr="ones",
                                 initial_distr="uniform",
                                 col_stoch=False,
                                 N_edges=N_edges,
                                 verbose=False,
                                 update_rule="HK",
                                 loops=True,
                                 static_graph=True,
                                 static_traits=True,
                                 **kwargs
                                 )
            avg = 0
            for realization in range(realizations):

                if realization%200==0:
                    print("\n==========================================================================================")
                    print(f"Calculating dynamics for epsilon={eps} and N_edges={N_edges} and realization={realization}.")
                    print("==========================================================================================\n")

                opinions_t = OD.time_evolution_opinions(realization=realization, plot=plot_ss, eps=eps, load=True, **kwargs)

                # Get number of clusters:
                clusters, _ = utils.find_clusters(opinions_t[-1,:], "Simple")
                avg += clusters/realizations
                OD.reset_graph()
                OD.reset_dynamics()

            cluster_avg_arr[eps_idx, N_edges_idx] = avg

    # Plotting:
    fig = plt.figure(figsize=(8,6))
    ax = fig.add_subplot(111)
    
    colors = plt.cm.rainbow(np.linspace(1,0,len(N_edges_array)))
    for N_edges_idx, N_edges in enumerate(N_edges_array):
        plot = ax.plot(eps_array, cluster_avg_arr[:,N_edges_idx], color=colors[N_edges_idx], label=r"$N_{\mathrm{edges}}=$%s"%(N_edges), lw=3)

    ax.set_xlabel(r"$\epsilon$")
    ax.set_ylabel(r"Average cluster number")
    ax.set_title(r"Steady state clusters in Hegselmann-Krause model for %s agents"%(N_actors))

    plt.tight_layout()
    plt.legend(loc='best')
    #plt.savefig("plots/avg-cluster-num-simply-connected-graph-N-actors-{0}-eps-{1}-{2}-{3}-N-edges-{4}-{5}-{6}-realizations-{7}.png".format(N_actors,np.round(eps_array[0],2),np.round(eps_array[-1],2), len(eps_array), np.round(N_edges_array[0],2), np.round(N_edges_array[-1],2), len(N_edges_array), realizations))
    plt.show()
    plt.close()

    return




######################################################################################

