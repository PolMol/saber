#!/usr/bin/env python

##########################################################################################
# This python program implements a simple model of univariate opinion dynamics
###########################################################################################


from __future__ import division
from __future__ import print_function

import numpy as np
from numpy import tanh, sign
from scipy.signal import argrelextrema
import scipy.linalg as spla

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import rc
from matplotlib import colors

from pylab import * #for movies
import time #for movies
import matplotlib.animation as animation #for movies

import datetime as dt

import random as ra

import sys

import networkx as nx 

from sklearn.cluster import MeanShift, estimate_bandwidth, KMeans
from sklearn.neighbors import KernelDensity
from sklearn.metrics import silhouette_score

from itertools import groupby



__author__      =   "Paolo Molignini"
__copyright__   =   "Copyright 2022, University of Stockholm"
__version__     =   "1.0.1"




################################
######     SUBROUTINES    ######
################################
######################################################################################
def get_number_of_neighbors(opinions, eps, i, kappa=100, smoothening=True):

    N = len(opinions)
    oi = opinions[i]
    num = 0

    for j in range(N):
        oj = opinions[j]


        if smoothening==True:

            num += (1 + tanh(kappa*eps - kappa*np.abs(oi - oj)))

        else:
            if np.abs(oj - oi) < eps:
                num += 1

    return num

######################################################################################


######################################################################################
def two_cluster_discrete(n, mu1, mu2, sig1, sig2, minval, maxval, plot, verbose=False):
    """
    Generates a discrete probability distribution formed of two separate ~Gaussian clusters.
    

    References
    ----------
    https://stackoverflow.com/questions/36894191/how-to-get-a-normal-distribution-within-a-range-in-numpy/36894456#36894456
    https://stackoverflow.com/questions/54609381/how-to-sample-from-a-truncated-gaussian-distribution-without-using-scipy

    """


    # def get_truncated_normal(mean=0, sd=1, low=0, upp=10):
    #     return ss.truncnorm((low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)
    # XL = get_truncated_normal(mean=2, sd=0.1, low=0, upp=15)
    # XR = get_truncated_normal(mean=5, sd=0.1, low=0, upp=15)
    # plt.hist(np.concatenate((XL.rvs(10000),XR.rvs(10000))))

    def double_normal(x, mu1, mu2, sig1, sig2):
        """ Sum of two normal distributions. Assumes mu1 < mu2."""

        assert mu1<mu2

        return 1. / (np.sqrt(2 * np.pi) * sig1) * np.exp(-0.5 * np.square(x - mu1) / np.square(sig1)) \
           + 1. / (np.sqrt(2 * np.pi) * sig2) * np.exp(-0.5 * np.square(x - mu2) / np.square(sig2))


    def trunc_double_normal(x, mu1, mu2, sig1, sig2, bounds=(minval, maxval)):
        if bounds is None: 
            bounds = (-np.inf, np.inf)

        norm = double_normal(x, mu1, mu2, sig1, sig2)
        norm[x < bounds[0]] = 0
        norm[x > bounds[1]] = 0

        return norm

    def sample_trunc(n, mu1, mu2, sig1, sig2, bounds, plot=False):
        """ Sample `n` points from truncated normal distribution """
        
        x = np.linspace(mu1 - 5. * sig1, mu2 + 5. * sig2, 10000)
        y = trunc_double_normal(x, mu1, mu2, sig1, sig2, bounds)
        #print(y)
        y_cum = np.cumsum(y) / y.sum()

        yrand = np.random.rand(n)
        sample = np.interp(yrand, y_cum, x)

        if plot==True:
            plt.hist(sample)
            plt.show()

        return sample

    # Simple test case with hard coded probability distribution:
    #test_2cluster_pd = np.array([20, 30, 70, 60, 50, 70, 50, 40, 35, 30, 26, 22, 19, 16, 14, 12])
    #test_2cluster_pd /= np.sum(test_2cluster_pd)

    samples = sample_trunc(n=n, mu1=mu1, mu2=mu2, sig1=sig1, sig2=sig2, bounds=(minval, maxval), plot=plot)
    for idx, s in enumerate(samples):
        samples[idx] = int(np.round(s))
        #print(s)

    return samples
######################################################################################


###########################################################################################
def find_clusters(X, method):
    """
    
    References:
    -----------

    https://stackoverflow.com/questions/51487549/unsupervised-learning-clustering-1d-array
    https://stackoverflow.com/questions/71677834/is-there-any-way-to-group-the-close-numbers-of-a-list-by-numpy 
    https://www.analyticsvidhya.com/blog/2021/05/k-mean-getting-the-optimal-number-of-clusters/
    https://stackoverflow.com/questions/35094454/how-would-one-use-kernel-density-estimation-as-a-1d-clustering-method-in-scikit/35151947#35151947

    """

    ########################################################################################
    # This is to check whether the data is all the same (1 cluster):
    g = groupby(X)
    crit = next(g, True) and not next(g, False)
    if crit==True:
        n_clusters_ = 1
        cluster_centers = X[0]
        return n_clusters_, cluster_centers
    # Check whether all the data points are within the same small threshold from each other:
    X6 = np.sort(X)
    if np.abs(X6[-1] - X6[0]) < 0.001:
        n_clusters_ = 1
        cluster_centers = X[0]
        return n_clusters_, cluster_centers
    ########################################################################################

    if method=="Simple":
        ########## METHOD 1 ##########
        # Arrange data into groups where successive elements
        # differ by no more than a small gap.
        ##############################

        maxgap = 0.001
        X1 = X
        X1.sort()
        groups = [[X1[0]]]
        for x in X1[1:]:
            if abs(x - groups[-1][-1]) <= maxgap:
                groups[-1].append(x)
            else:
                groups.append([x])

        n_clusters_, cluster_centers = calculate_cluster_centers(groups)


    elif method=="KernelDensity":

        ########## METHOD 2 ##########
        # Use Kernel Density Algorithm 
        # from external library:
        ##############################
        try:

            X4 = X.reshape(-1, 1)
            kde = KernelDensity(kernel='gaussian', bandwidth=3).fit(X4)
            s = np.linspace(0, 1.0, 10*len(X4))
            e = kde.score_samples(s.reshape(-1,1))

            mi, ma = argrelextrema(e, np.less)[0], argrelextrema(e, np.greater)[0]
            print("Minima:", s[mi])
            print("Maxima:", s[ma])

            n_clusters_ = len(s[ma])
            cluster_centers = s[ma]
        except:
            print(f"Kernel Density algorithm failed.")
            method = "MeanShift"


    if method=="MeanShift":
        ########## METHOD 3 ##########
        # MeanShift algorithm.
        ##############################

        try:
            X2 = np.reshape(X, (-1, 1))

            # Compute clustering with MeanShift

            # The following bandwidth can be automatically detected using
            #bandwidth = estimate_bandwidth(X2, quantile=0.1, n_samples=100)
            bandwidth = None

            ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)
            ms.fit(X2)
            labels = ms.labels_
            cluster_centers = ms.cluster_centers_

            labels_unique = np.unique(labels)
            n_clusters_ = len(labels_unique)

        except:
            print(f"MeanShift algorithm failed.")
            method = "KMeans"

    if method=="KMeans":
        try:
            print(f"Trying K-Means algorithm with best guess given by silhouette method.")
            X3 = sorted(X)
            data = np.reshape(X3,(-1,1))

            range_n_clusters = list(range(2,20))
            silhouette_avg = []
            list_of_cluster_lists = []
            for num_clusters in range_n_clusters:

                # initialise kmeans
                kmeans = KMeans(n_clusters=num_clusters, random_state=0).fit(data)
                cluster_labels = kmeans.labels_
                clusters = [[i[0] for i in list(d)] for g,d in groupby(list(zip(X3,cluster_labels)), key=lambda x: x[1])]
                list_of_cluster_lists.append(clusters)

                # silhouette score
                silhouette_avg.append(silhouette_score(data, cluster_labels))
        
            print(f"Sihlouette average scores: {silhouette_avg}")
            # find maximal value of silhouette score and its index:
            n_opt = np.argmax(silhouette_avg)
            cluster_list = list_of_cluster_lists[n_opt]
            n_clusters_, cluster_centers = calculate_cluster_centers(cluster_list)

        except:
            print(f"K-Means algorithm failed.")
            method="LastResort"

    if method=="LastResort":
        print(f"Last resort. Trying simple sorting and grouping based on largest gaps. This result might not be accurate.")
            
        cluster_list = []
        cluster = []
        X5 = np.sort(X)
        for i in range(len(X5)-1):
            cluster.append(X5[i])
            if X5[i+1] - X5[i] > 0.001: # there is a jump to the new cluster
                cluster_list.append(cluster)
                cluster = []

        # Check last item in the list:            
        if X5[-1] - X5[-2] > 0.001:
            cluster = []
        cluster.append(X5[-1])
        cluster_list.append(cluster)

        n_clusters_, cluster_centers = calculate_cluster_centers(cluster_list)

    return n_clusters_, cluster_centers
###########################################################################################


###########################################################################################
def calculate_cluster_centers(cluster_list):
    """
    
    
    """
    n_clusters_ = len(cluster_list)
    cluster_centers = []
    for idx in range(n_clusters_):
        cluster_centers.append(np.average(cluster_list[idx]))
    return n_clusters_, cluster_centers
###########################################################################################




################################
######      PLOTTERS      ######
################################
######################################################################################
def plot_Jacobian_HK(J, timestep):
    """
    """

    fig = plt.figure(figsize=(8,6))
    ax = fig.add_subplot(111)

    heatmap = ax.imshow(J, cmap="inferno")
    plt.colorbar(heatmap)

    ax.set_xlabel(r"$i$")
    ax.set_ylabel(r"$j$")
    ax.set_title(r"Jacobian at time $t=$%s"%(timestep))

    plt.tight_layout()
    plt.show()
    plt.close()

    return
######################################################################################