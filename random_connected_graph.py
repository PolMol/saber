# Adapted from:

# Copyright (C) 2013 Brian Wesley Baugh
# CSCE 6933: Social Network Analysis
# Created: January 22, 2013
# Updated: January 30, 2013

# Paolo Molignini
# Stockholm University
# Created: 8 February 2023

"""Generate a randomly connected graph with N nodes and E edges."""
import random
import argparse
from pprint import pprint
import numpy as np
import utils
import sys
import time

class Graph(object):
    def __init__(self, nodes, edges=None, loops=False, multigraph=False,
                 digraph=False):
        """
        Parameters:
        ----------
        nodes, : a set of nodes to link to form the graph.

        edges, : a set of already provided edges between the nodes (can be None).

        loops, boolean: flag to determine whether loops (edges from a node to itself) are allowed.
        
        multigraph, boolean: flag for constructing multigraph (a graph in which the same nodes can be connected by multiple, different edges).

        digraph, boolean: flag for constructing a directed graph (e.g. 1 --> 2 is different than 2 --> 1).


        """
        self.nodes = nodes
        if edges:
            self.edges = edges
            self.edge_set = self._compute_edge_set()
        else:
            self.edges = []
            self.edge_set = set()
        self.loops = loops
        self.multigraph = multigraph
        self.digraph = digraph

    def _compute_edge_set(self):
        raise NotImplementedError()

    def add_edge(self, edge):
        """Add the edge if the graph type allows it."""
        if self.multigraph or edge not in self.edge_set:
            self.edges.append(edge)
            self.edge_set.add(edge)
            if not self.digraph:    # if the Graph is NOT a directed graph, add the edge in the opposite direction to the set.
                self.edge_set.add(edge[::-1])  
            return True
        return False

    def make_random_edge(self):
        """Generate a random edge between any two nodes in the graph."""
        if self.loops:
            # Including self-edges:
            random_edge = (random.choice(self.nodes), random.choice(self.nodes))
        else:
            # Without self-edges:
            random_edge = tuple(random.sample(self.nodes, 2))
        return random_edge

    def add_random_edges(self, total_edges):
        """Add random edges until the number of desired edges is reached."""
        while len(self.edges) < total_edges:
            self.add_edge(self.make_random_edge())

    def sort_edges(self):
        """If undirected, sort order that the nodes are listed in the edge."""
        if not self.digraph:
            self.edges = [((t, s) if t < s else (s, t)) for s, t in self.edges]
        self.edges.sort()

    def generate_gml(self):
        """
        Generate representation of graph as GML (Graph Modelling Language), a sort of dictionary containing the properties of the graph.
        It enumerates all nodes and edges with their indices, properties, etc.

        References:
        ---------
        https://networkx.org/documentation/stable/reference/readwrite/generated/networkx.readwrite.gml.generate_gml.html
        https://web.archive.org/web/20190303094704/http://www.fim.uni-passau.de/fileadmin/files/lehrstuhl/brandenburg/projekte/gml/gml-technical-report.pdf

        Notes:
        -----

        The "yield" statement is used to iterate over generators (e.g. lazy iterators that you can loop over, list-like objects) or infinite lists without storing the entire array of data.
        It is used to circumvent memory overflow errors when reading and manipulating large files/lists/etc.

        """
        # Inspiration:
        # http://networkx.lanl.gov/_modules/networkx/readwrite/gml.html#generate_gml
        indent = ' ' * 4

        yield 'graph ['
        if self.digraph:
            yield indent + 'directed 1'

        # Write nodes
        for index, node in enumerate(self.nodes):
            yield indent + 'node ['
            yield indent * 2 + 'id {}'.format(index)
            yield indent * 2 + 'label "{}"'.format(str(node))
            yield indent + ']'

        # Write edges
        for source, target in self.edges:
            yield indent + 'edge ['
            yield indent * 2 + 'source {}'.format(self.nodes.index(source))
            yield indent * 2 + 'target {}'.format(self.nodes.index(target))
            yield indent + ']'

        yield ']'

    def write_gml(self, fname):
        """
        Write GML file for the graph.
        """
        with open(fname, mode='w') as f:
            for line in self.generate_gml():
                line += '\n'
                f.write(line)#.encode('latin-1'))








######################################################################################
def simply_connected_graph(N_nodes, 
                           N_edges,
                           loops,
                           bond_type, 
                           method="naive",
                           verbose=False):

    """
    Construct a simply connected graph describing the connections between the actors via random_connected_graph module.


    """

    if bond_type=="directed":
        digraph=True
    elif bond_type=="undirected":
        digraph=False

    nodes = []
    for n in range(N_nodes):
        nodes.append(n)

    if verbose==True:
        start_time = time.time()

    if method=="naive":
        print(f"The graph assembling method \"{method}\" is slow!")
        graph = naive(nodes=nodes,
                      num_edges=N_edges,
                      loops=False,
                      multigraph=False,
                      digraph=digraph)
        print(f"Graph assembled.")


    elif method=="Wilson":
        # Generate a minimal random spanning tree with Wilson's algorithm.
        # This method should not have biases.
        graph = wilsons_algo(nodes=nodes,
                             num_edges=N_edges,
                             loops=False, 
                             multigraph=False,
                             digraph=digraph)

    elif method=="random_walk":
        graph = random_walk(nodes=nodes, 
                            num_edges=N_edges, 
                            loops=False, 
                            multigraph=False, 
                            digraph=digraph)

    elif method=="partition":
        print(f"The method {method} is basied towards more clustered graphs!")
        graph = partition(nodes=nodes, 
                          num_edges=N_edges, 
                          loops=False, 
                          multigraph=False, 
                          digraph=digraph)   

    if loops==True:
        for node in nodes:
            graph.add_edge((node,node))

    if verbose==True:
        print("Graph assembled in  %s seconds." % (time.time() - start_time))
    return graph

######################################################################################


######################################################################################
def complete_graph(N_nodes,
                   loops,
                   ):

    """
    Construct a complete graph.

    """

    nodes = []
    for n in range(N_nodes):
        nodes.append(n)

    graph = Graph(nodes=nodes, 
                  loops=loops, 
                  multigraph=False, 
                  digraph=False)

    for i in range(N_nodes):
        for j in range(N_nodes):
            if loops==True:
                if i!=j:
                    graph.add_edge((i,j))
                elif i==j and (i,j) not in graph.edges:
                    graph.add_edge((i,j))

            elif loops==False and i!=j:
                graph.add_edge((i,j))

    return graph

######################################################################################

######################################################################################
def two_cluster_graph(N_nodes,
                      loops,
                      bond_type,
                      verbose
                      ):

    """
    Construct a graph with the number of neighbors of each node following a two-cluster distribution.

    """

    if bond_type=="directed":
        digraph=True
    elif bond_type=="undirected":
        digraph=False

    nodes = []
    for n in range(N_nodes):
        nodes.append(n)

    graph = Graph(nodes=nodes, 
                  loops=loops, 
                  multigraph=False, 
                  digraph=digraph)
    
    # Loop over all actors w/ index j>i
    for i in range(N_nodes-1):

    # Get a random number of friends according to given distribution:
        idcs = get_random_neighbors(i=i, graph=graph, graph_type="2cluster", digraph=digraph, verbose=verbose)

        for j in idcs:
            if loops==True:
                graph.add_edge((i,j))
            elif loops==False and i!=j:
                graph.add_edge((i,j))

    return graph

######################################################################################

######################################################################################
def uniform_random_graph(N_nodes,
                      loops,
                      bond_type,
                      verbose
                      ):

    """
    Construct a uniformly random graph.

    """

    if bond_type=="directed":
        digraph=True
    elif bond_type=="undirected":
        digraph=False

    nodes = []
    for n in range(N_nodes):
        nodes.append(n)

    graph = Graph(nodes=nodes, 
                  loops=loops, 
                  multigraph=False, 
                  digraph=digraph)
    
    # Loop over all actors w/ index j>i
    for i in range(N_nodes-1):

    # Get a random number of friends according to given distribution:
        idcs = get_random_neighbors(i=i, graph=graph, graph_type="uniform-random", digraph=digraph, verbose=verbose)

        for j in idcs:
            if loops==True:
                graph.add_edge((i,j))
            elif loops==False and i!=j:
                graph.add_edge((i,j))

    return graph

######################################################################################


######################################################################################
def graph_from_adj(adj,
                   ):

    """
    Construct a graph from the given adjacency matrix (does not work for multigraphs!)

    """

    N_nodes = np.shape(adj)[0]
    nodes = []
    for n in range():
        nodes.append(n)

    # Determine if there are self-edges:
    loops = False
    for i in range(N_nodes):
        if adj[i,i] != 0:
            loops = True
            break

    # Determine if the graph is directed or not:
    digraph = False
    for i in range(N_nodes):
        for j in range(N_nodes):
            if adj[i,i] != adj[j,i]:
                digraph = True
                break

    # Initialize graph:
    graph = Graph(nodes=nodes, 
                  loops=loops, 
                  multigraph=False, 
                  digraph=False)

    for i in range(N_nodes):
        for j in range(N_nodes):
            if adj[i,j]!=0:
                graph.add_edge((i,j))

    return graph

######################################################################################

######################################################################################
def get_random_neighbors(i,
                         graph,
                         graph_type,
                         digraph,
                         verbose):
    """
    Initializes a random number of neighbors for node i.
    
    """
    
    N_nodes = len(graph.nodes)

    if verbose==True:
        print("\n=====================")
        print(f"Working on index {i}")
        print("=====================\n")

    # Get current edges pointing TO element i
    existing_edges = len([t[0] for t in graph.edges if t[1] == i])

    if digraph==False:
        # Generate a random number of neighbors:
        if verbose==True:
            print(f"minval={existing_edges}, maxval={N_nodes-1}")
        if graph_type=="2cluster":
            s = int(utils.two_cluster_discrete(n=1, mu1=2, mu2=5, sig1=0.5, sig2=1.0, minval=existing_edges, maxval=N_nodes-1, plot=False, verbose=False)[0])
            if verbose==True:
                print(f"s={s}")

            # Generate the list of random indices:
            idcs = random.sample(range(i+1, N_nodes), s-existing_edges)
            idcs = np.array(idcs)


    elif digraph==True:
        # Generate a random number of neighbors:
        if graph_type=="2cluster":
            s = int(utils.two_cluster_discrete(n=1, mu1=2, mu2=5, sig1=0.5, sig2=1.0, minval=0, maxval=N_nodes-1, plot=False, verbose=verbose)[0])
        elif graph_type=="uniform-random":
            s = random.randint(0,N_nodes-1)
        else:
            print("This option for random neighbor generation is not implemented yet / recognized!")
            sys.exit()

        # Get the random indices:
        idcs = random.sample(list(range(0,i))+list(range(i+1,N_nodes)), s)
        # Always add "self-bond" to avoid divergences when normalizing:
        idcs.append(i)

    return idcs
######################################################################################



######################################################################################
def check_num_edges(nodes, num_edges, loops, multigraph, digraph):
    """Checks that the number of requested edges is acceptable."""
    num_nodes = len(nodes)
    # Check min edges
    min_edges = num_nodes - 1
    if num_edges < min_edges:
        raise ValueError('num_edges less than minimum (%i)' % min_edges)
    # Check max edges
    max_edges = num_nodes * (num_nodes - 1)
    if not digraph:
        max_edges /= 2
    if loops:
        max_edges += num_nodes
    if not multigraph and num_edges > max_edges:
            raise ValueError('num_edges greater than maximum (%i)' % max_edges)
######################################################################################



######################################################################################
def naive(nodes, num_edges, loops=False, multigraph=False, digraph=False):
    """
    Constructs graph in a naive way by keeping track of fully connected components in the graph.
    

    Notes:
    ------
    
    Each node starts off in its own component.
    Keep track of the components, combining them when an edge merges two.
    While there are less edges than requested:
        Randomly select two nodes, and create an edge between them.
    If there is more than one component remaining, repeat the process.
    
    """

    check_num_edges(nodes, num_edges, loops, multigraph, digraph)

    def update_components(components, edge):
        # Update the component list.
        comp_index = [None] * 2
        for index, comp in enumerate(components):
            for i in (0, 1):
                if edge[i] in comp:
                    comp_index[i] = index
            # Break early once we have found both sets.
            if all(x is not None for x in comp_index):
                break
        # Combine components if the nodes aren't already in the same one.
        if comp_index[0] != comp_index[1]:
            components[comp_index[0]] |= components[comp_index[1]]
            del components[comp_index[1]]

    finished = False
    while not finished:
        graph = Graph(nodes, loops=loops, multigraph=multigraph, digraph=digraph)
        # Start with each node in its own component.
        components = [set([x]) for x in nodes]
        while len(graph.edges) < num_edges:
            # Generate a random edge.
            edge = graph.make_random_edge()
            if graph.add_edge(edge):
                # Update the component list.
                update_components(components, edge)
        if len(components) == 1:
            finished = True

    return graph
######################################################################################


######################################################################################
def partition(nodes, num_edges, loops=False, multigraph=False, digraph=False):
    # Algorithm inspiration:
    # http://stackoverflow.com/questions/2041517/random-simple-connected-graph-generation-with-given-sparseness

    # Idea:
    # Create a random connected graph by adding edges between nodes from
    # different partitions.
    # Add random edges until the number of desired edges is reached.

    # This method iteratively adds nodes and edges, but it's biased towards more clustered graphs

    check_num_edges(nodes, num_edges, loops, multigraph, digraph)

    graph = Graph(nodes, loops=loops, multigraph=multigraph, digraph=digraph)

    # Create two partitions, S and T. Initially store all nodes in S.
    S, T = set(nodes), set()

    # Randomly select a first node, and place it in T.
    node_s = random.sample(sorted(S), 1).pop()
    S.remove(node_s)
    T.add(node_s)

    # Create a random connected graph.
    while S:
        # Select random node from S, and another in T.
        node_s, node_t = random.sample(sorted(S), 1).pop(), random.sample(sorted(T), 1).pop()
        # Create an edge between the nodes, and move the node from S to T.
        edge = (node_s, node_t)
        assert graph.add_edge(edge) == True
        S.remove(node_s)
        T.add(node_s)

    # Add random edges until the number of desired edges is reached.
    graph.add_random_edges(num_edges)

    return graph

######################################################################################


######################################################################################
def random_walk(nodes, num_edges, loops=False, multigraph=False, digraph=False):
    # Algorithm inspiration:
    # https://en.wikipedia.org/wiki/Uniform_spanning_tree#The_uniform_spanning_tree

    # Idea:
    # Create a uniform spanning tree (UST) using a random walk.
    # Add random edges until the number of desired edges is reached.

    check_num_edges(nodes, num_edges, loops, multigraph, digraph)

    # Create two partitions, S and T. Initially store all nodes in S.
    S, T = set(nodes), set()

    # Pick a random node, and mark it as visited and the current node.
    current_node = random.sample(sorted(S), 1).pop()
    S.remove(current_node)
    T.add(current_node)

    graph = Graph(nodes, loops=loops, multigraph=multigraph, digraph=digraph)

    # Create a random connected graph.
    while S:
        # Randomly pick the next node from the neighbors of the current node.
        # As we are generating a connected graph, we assume a complete graph.
        neighbor_node = random.sample(nodes, 1).pop()
        # If the new node hasn't been visited, add the edge from current to new.
        if neighbor_node not in T:
            edge = (current_node, neighbor_node)
            graph.add_edge(edge)
            S.remove(neighbor_node)
            T.add(neighbor_node)
        # Set the new node as the current node.
        current_node = neighbor_node

    # Add random edges until the number of desired edges is reached.
    graph.add_random_edges(num_edges)

    return graph
######################################################################################


######################################################################################
def wilsons_algo(nodes, num_edges, loops=False, multigraph=False, digraph=False, verbose=False):

    """
    Implements Wilson's algorithm to generate a random connected graph.


    References:
    -----------
    https://en.wikipedia.org/wiki/Uniform_spanning_tree#The_uniform_spanning_tree
    https://en.wikipedia.org/wiki/Loop-erased_random_walk
    David Bruce Wilson, Generating random spanning trees more quickly than the cover time, STOC '96: Proceedings of the twenty-eighth annual ACM symposium on Theory of Computing (1996), Pages 296–303 https://doi.org/10.1145/237814.237880
    
    Notes:
    ------
    
    Steps:
    1) create a uniform spanning tree (UST):
        - Start with a single-vertex tree T.
        - Until there are no more vertices v that are not in T:
            - Pick a random vertex v not in T.
            - Perform a (loop-erased) random walk from v until you hit T.
            - Add the nodes and edges along the random walk to T.
    2) Add random edges to the UST until the number of desired edges is reached.


    """

    # Initial check
    check_num_edges(nodes, num_edges, loops, multigraph, digraph)

    # Create three sets:
    # R keeps track of the current (loop-erased) random walk
    # S keeps track of the available nodes
    # T keeps track of the spanning tree
    R, S, T = set(), set(nodes), set()

    #  First, construct a single-vertex tree T by choosing (arbitrarily) one vertex:
    initial_node = random.sample(sorted(S), 1).pop()
    if verbose==True:
        print(f"initial_node: {initial_node}")
    S.remove(initial_node)
    T.add(initial_node)

    # Pick a random node (not in T):
    current_node = random.sample(sorted(S), 1).pop()

    # Initialize the graph:
    graph = Graph(nodes, loops=loops, multigraph=multigraph, digraph=digraph)

    # Loop over all random walks to T until the set S is empty:    
    while(S != set() or R != set()):
        # Add new nodes to the current random walk until you reach a node in T:

        R_vertices = [current_node]
        while current_node not in T:
            
            if verbose==True:
                print(f"R: {R}")
                print(f"S: {S}")
                print(f"T: {T}")

            # Randomly pick the next node from all nodes:
            neighbor_node = random.sample(nodes, 1).pop()

            if verbose==True:
                print(f"current_node: {current_node}")
                print(f"neighbor_node: {neighbor_node}")


            # TODO: Need to modify this part to make a proper loop-erased random walk:
            # - Keep adding edges to R (even with loops) until we hit T.
            # - remove all the loops in R.
            # - Merge R into T

            R_vertices.append(neighbor_node)
            current_node = neighbor_node
        
        #print(f"R_vertices: {R_vertices}")
        # Now we have a random walk from some S point to T.
        R_vertices_LE = loop_erasing(R_vertices)
        #print(f"R_vertices_LE: {R_vertices_LE}")

        for i in range(len(R_vertices_LE)-1):
            edge = (R_vertices_LE[i], R_vertices_LE[i+1])
            graph.add_edge(edge)
            S.remove(R_vertices_LE[i])
            R.add(R_vertices_LE[i])
        
        # Add the entire random walk R to T, then reset R:
        T.update(R)
        R = set()

        if verbose==True:
            print(f"graph: {graph.edges}")

        # Restart a new random walk in the remaining nodes in S:
        if S != set(): 
            current_node = random.sample(sorted(S), 1).pop()
            R.add(current_node)

        if verbose==True:
            print(f"R: {R}")
            print(f"S: {S}")
            print(f"T: {T}")
    
    # Add random edges until the number of desired edges is reached.
    graph.add_random_edges(num_edges)

    return graph

######################################################################################



######################################################################################
def check_bias(repetitions, method):
    

    """
    Checks bias of the generated graph.
    """
    N_nodes = 4
    N_edges = 3

    star_count = 0
    for i in range(repetitions):

        graph = simply_connected_graph(N_nodes=N_nodes, N_edges=N_edges, loops=False, bond_type="undirected", method=method, verbose=False)

        for j in range(4):
            counter=0 
            for edge in graph.edges:
                if j in edge:
                    counter += 1
                if counter==3:
                    star_count += 1
                    break


    print(f"Expected frequency of star graph: 25%")
    print(f"Actual frequency of star graph: {star_count/repetitions*100}%")
    if np.abs(star_count/repetitions*100 - 25.0) > 1:
        print(f"The graph generator is biased!")
    else:
        print(f"The graph generator is NOT biased.")


######################################################################################

######################################################################################
def loop_erasing(vertex_list):
    
    vertex_list_LE = [vertex_list[0]]
    r = vertex_list[0]
    while r != vertex_list[-1]:

        # Get the maximum index j in R_vertices such that R_vertices[j] = r, 
        # and then take the index right after that:
        max_idx = np.amax([idx for idx in range(len(vertex_list)) if vertex_list[idx]==r]) 
        next_idx = max_idx + 1
        # Append the vertex corresponding to the index right after the maximum.
        # This erases any loops to the next vertex.
        vertex_list_LE.append(vertex_list[next_idx])
        r = vertex_list[next_idx]

    return vertex_list_LE
######################################################################################






if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('nodes',
                        help='filename containing node labels (one per line) '
                             'OR integer number of nodes to generate')
    parser.add_argument('-e', '--edges', type=int,
                        help='number of edges (default is minimum possible)')
    parser.add_argument('-l', '--loops', action='store_true',
                        help='allow self-loop edges')
    parser.add_argument('-m', '--multigraph', action='store_true',
                        help='allow parallel edges between nodes')
    parser.add_argument('-d', '--digraph', action='store_true',
                        help='make edges unidirectional')
    parser.add_argument('-w', '--wilson', action='store_const',
                        const='wilsons_algo', dest='approach',
                        help="use wilson's generation algorithm (best)")
    parser.add_argument('-r', '--random-walk', action='store_const',
                        const='random_walk', dest='approach',
                        help='use a random-walk generation algorithm (default)')
    parser.add_argument('-n', '--naive', action='store_const',
                        const='naive', dest='approach',
                        help='use a naive generation algorithm (slower)')
    parser.add_argument('-t', '--partition', action='store_const',
                        const='partition', dest='approach',
                        help='use a partition-based generation algorithm (biased)')
    parser.add_argument('--no-output', action='store_true',
                        help='do not display any output')
    parser.add_argument('-p', '--pretty', action='store_true',
                        help='print large graphs with each edge on a new line')
    parser.add_argument('-g', '--gml',
                        help='filename to save the graph to in GML format')
    args = parser.parse_args()

    # Nodes
    try:
        nodes = []
        with open(args.nodes) as f:
            for line in f:
                nodes.append(line.strip())
    except IOError:
        try:
            nodes = [x for x in xrange(int(args.nodes))]
        except ValueError:
            raise TypeError('nodes argument must be a filename or an integer')

    # Edges
    if args.edges is None:
        num_edges = len(nodes) - 1
    else:
        num_edges = args.edges

    # Approach
    if args.approach:
        print("Setting approach:", args.approach)
        approach = locals()[args.approach]
    else:
        approach = random_walk

    # Run
    graph = approach(nodes, num_edges, args.loops, args.multigraph,
                     args.digraph)

    # Display
    if not args.no_output:
        graph.sort_edges()
        if args.pretty:
            pprint(graph.edges)
        else:
            print(graph.edges)

    # Save to GML
    if args.gml:
        graph.write_gml(args.gml)